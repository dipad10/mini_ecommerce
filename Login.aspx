﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <style>
        .login-form {
            width: 25rem;
            height: 20.75rem;
            /*position: fixed;*/
            /*position:absolute;*/
             margin: 0 auto;
            /*margin-top: -9.375rem;*/
           
            /*margin-left: -12.5rem;*/
            background-color: #ffffff;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
            border-radius:10px;
           
        }
    </style>
    <script>
            $(function () {
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });
        });
    </script>
  
    <div style="box-shadow: 1px 3px 50px 3px gray;" class="login-form animated fadeInUp padding20 block-shadow" style="opacity: 1; transform: scale(1); transition: 0.5s;">

            <h1 class="text-light">Login</h1>
            <hr class="thin">
       
            <div class="input-control modern text iconic full-size">
                <asp:TextBox ID="txtusername" runat="server"></asp:TextBox>
                <span class="label">Username</span>
                <span class="informer">Please enter your Username</span>
                <span class="placeholder">Enter Username</span>
                <span class="icon mif-user"></span>
            </div>


            <br>
           
            <div class="input-control modern password iconic full-size" data-role="input">
                <asp:TextBox ID="txtpassword" TextMode="Password" runat="server"></asp:TextBox>
                <span class="label">Password</span>
                <span class="informer">Please enter your password</span>
                <span class="placeholder">Input password</span>
                <span class="icon mif-lock"></span>
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>

            
            <br>
            <div style="text-align: center;">
                <asp:RadioButton ID="RadioButton1" Text="Remember me" CssClass=" text-small " runat="server" />
                |
                <asp:HyperLink ID="HyperLink2" NavigateUrl="#" CssClass="text-small" runat="server">Forgot password?</asp:HyperLink>

            </div>

            <br>
          

            <div class="form-actions">
                <asp:Button ID="Button1" OnClick="Button1_Click" CssClass="button full-size bg-darkBrown rounded fg-white" runat="server" Text="Enter Your Account" />

            </div>
            <div style="text-align: center;" class="form-actions">
                <p class="text-small">Don't have an account? <a class="text-small link" href="register.aspx">Create One</a></p>
            </div>
        </div>
</asp:Content>

