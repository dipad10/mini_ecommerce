﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            if(Session["username"] != null)
            {
                welcome.InnerHtml = string.Format("Welcome, {0}", Session["username"]);
                lblloggoff.Visible = true;
                myorders.Visible = true;
            }
        }
    }

    protected void btnviewcart_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("/cart.aspx");
    }
}
