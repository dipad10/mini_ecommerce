﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BlockDetails.aspx.cs" Inherits="BlockDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <h6 class="fg-blue margin20">Blocks Details</h6>
    <br />
     <script>
                $(document).ready(function () {
                    $(".notify-closer").click(function () {
                        $("#messagebox").hide();
                    });
                });
            </script>
            <div runat="server" style="width:100%;" id="messagebox" visible="false" class="notify success">
                            <span class="notify-closer"></span>
                            <span class="notify-title">Successful</span>
                            <span class="notify-text text-small ">Product added to Cart Successfully!</span>
                        </div>
       <div style="margin-left:20px;" class="grid margin20">
            <div class="row cells3">
                <asp:DataList ID="DataList1" runat="server">
                    <ItemTemplate>
   <div class="cell">                 
               
                      <asp:Label ID="ImageUrlLabel" runat="server" Text='<%# Eval("ImageUrl") %>' Visible="False"></asp:Label>   
        <asp:Label ID="lblproductid" runat="server" Text='<%# Eval("ProductID") %>' Visible="False"></asp:Label>            
                  <a href="BlockDetails.aspx?P=<%#Eval("ProductID")%>">
                       <img width="500" class="rounded" height="500" src="<%# Eval("ImageUrl")%>" />
                  </a> 
                     <asp:Label ID="NameLabel" runat="server" Visible="false" Text='<%# Eval("Name")%>'></asp:Label>
                                <asp:Label ID="PriceLabel" Visible="false" runat="server" Text='<%# Eval("Price")%>'></asp:Label>
                               <p class="text-secondary text-bold"><%# Eval("Name") %></p> 
       <p class="text-small"><%# Eval("Description") %></p>
       <p class="text-small text-bold"><%# Eval("Price")%></p>
       <p>
          <span>Specify number of Hectares: </span> <asp:TextBox ID="txtamt" placeholder="AMT" Width="50" runat="server"></asp:TextBox>
       </p>
                            <asp:Button ID="btnaddtocart" Visible="true" Height="29px" OnClick="btnaddtocart_Click" CssClass="button text-small bg-green fg-white mini-button" runat="server" Text="Add to cart" Font-Size="X-Small" Font-Bold="True" />                          
                </div>
                    </ItemTemplate>
                </asp:DataList>
                        
                  
             
                
            </div>
        </div>
</asp:Content>

