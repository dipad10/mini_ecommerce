﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    

      
            <h5 class="title margin20">Available Blocks For sale</h5>
           <asp:Label ID="lblerror" Visible="false" runat="server" Text=""></asp:Label>
           
             <style>
                @media(min-width: 992px) {
                    .col5 {
                        width: 20%;
                        float: left;
                        position: relative;
                        min-height: 1px;
                        padding-right: 15px;
                        padding-left: 15px;
                    }
                }

                
            </style>
            <%-- Here i added a repeater control to display the list of blocks from the database using databind --%>
        <div class="grid">
            <div class="row">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                           <div class="col5">  
                                <asp:Label ID="ImageUrlLabel" runat="server" Text='<%# Eval("ImageUrl") %>' Visible="False"></asp:Label>               
                  <a href="BlockDetails.aspx?P=<%#Eval("ProductID")%>">
                       <img width="300" class="rounded" height="350" src="<%# Eval("ImageUrl")%>" />
                  </a> 
                     <asp:Label ID="NameLabel" runat="server" Visible="false" Text='<%# Eval("Name")%>'></asp:Label>
                                <asp:Label ID="lblprocid" runat="server" Visible="false" Text='<%# Eval("ProductID")%>'></asp:Label>
                                <asp:Label ID="PriceLabel" Visible="false" runat="server" Text='<%# Eval("Price")%>'></asp:Label>
                               <p><%# Eval("Name") %></p> <p class="text-small"><%# Eval("Price", "{0:c}")%></p>

                   <a href="BlockDetails.aspx?P=<%#Eval("ProductID")%>" class="link text-small text-bold ">Add To Cart<i class="fa fa-shopping-cart"></i></a>
                </div>
                    </ItemTemplate>
                </asp:Repeater>
             
                
            </div>
        </div>
       <!-- End of tiles -->

    
    


</asp:Content>

