﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Checkout.aspx.cs" Inherits="Checkout" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <style>
        .register {
            width: 25rem;
          
            /*position: fixed;*/
            /*position:absolute;*/
             margin: 0 auto;
            /*margin-top: -9.375rem;*/
           
            /*margin-left: -12.5rem;*/
           
          
        }
    </style>
     <div class="margin20">

        <asp:GridView ID="grdCart" CssClass="table striped hovered" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductID" OnRowCancelingEdit="grdCart_RowCancelingEdit" OnRowDeleting="grdCart_RowDeleting" OnRowEditing="grdCart_RowEditing" OnRowUpdating="grdCart_RowUpdating">
        <AlternatingRowStyle />
        <Columns>
            <asp:TemplateField HeaderText="Serial No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="Image1" Width="50" Height="50" runat="server" ImageUrl='<%# Eval("ImageUrl")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductName" HeaderText="Product" ReadOnly="True" />
            <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
            <asp:BoundField DataField="Price" DataFormatString="{0:N}" HeaderText="Price" ReadOnly="True" />
            <asp:BoundField DataField="SubTotal" DataFormatString="{0:N}" HeaderText="Total"
                ReadOnly="True" />
            <asp:TemplateField HeaderText="Remove">
                <ItemTemplate>
                    <asp:Button ID="Button1" CommandName="Delete" CssClass="button text-small bg-green fg-white mini-button" Font-Size="X-Small" Font-Bold="True" runat="server" Text="Remove" />

                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField>

                <ItemTemplate>
                    <asp:ImageButton ImageUrl="/images/edit-icon.png" CommandName="Edit" runat="server"></asp:ImageButton>
                    <asp:Button ID="Button2" CommandName="Update" CssClass="button text-small bg-green fg-white mini-button" Font-Size="X-Small" Font-Bold="True" runat="server" Text="Update Cart" />
                </ItemTemplate>

            </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="false" ShowEditButton="false" />
        </Columns>
        <EmptyDataTemplate>
            Your Shopping Cart is empty, add items
        <a class="text-small" href="default.aspx">Add Products</a>
        </EmptyDataTemplate>
    </asp:GridView>
       <p>
            <asp:Label ID="TotalLabel" CssClass="margin20" runat="server" Font-Bold="True" Font-Size="15"></asp:Label>
       </p> 
      
         <div class="register">
            <h3 class="margin20">Billing Details & Payment</h3>
<hr />

             <br />
             <div class="panel" data-role="panel">
    <div class="heading">
        <span class="title">Personal Details</span>
    </div>
    <div class="content bg-white">
        <div class="margin20">
                    <p class="text-bold text-secondary">Fullname</p>
        <div class="input-control full-size text">
            <asp:TextBox ID="txtfullname" runat="server"></asp:TextBox>
</div>
      
                 <p class="text-bold text-secondary">E-mail</p>
        <div class="input-control full-size text">
            <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
</div>
        <p class="text-bold text-secondary">Mobile number</p>
        <div class="input-control full-size text" data-role="keypad">
  <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox>
</div>
      
        <p class="text-bold text-secondary">PostCode</p>
           <div class="input-control full-size text" data-role="keypad">
            <asp:TextBox ID="txtpostcode" runat="server"></asp:TextBox>
</div>

        <p class="text-bold text-secondary">Address</p>
           <div class="input-control full-size text">
            <asp:TextBox ID="txtaddress" runat="server"></asp:TextBox>
</div>
        </div>

           
    </div>
</div>
             <div class="panel" data-role="panel">
    <div class="heading">
        <span class="title">Make Payment</span>
    </div>
    <div class="content bg-white">
        <div class="margin20">
            <p>
                <img src="images/Credit-Card-Visa-And-Master-Card-Transparent-PNG.png" height="35" width="100%" />
            </p>
                     <p class="text-bold text-secondary">*Name On Card</p>
        <div class="input-control full-size text">
           <asp:TextBox ID="txtowner" placeholder="Enter Card Owner's name" runat="server"></asp:TextBox>
</div>
    <p class="text-bold text-secondary"> Card Number</p>
        <div class="input-control full-size text" data-role="keypad">
           <span class="mif-mastercard"></span> <asp:TextBox ID="txtmastercard" runat="server"></asp:TextBox>
</div>
        <p class="text-bold text-secondary">*Expiration Date</p>
        <div class="clearfix">
            <div class="place-left">
                        <div class="input-control size-p10 text">
                            <asp:DropDownList ID="ddlmonth" runat="server">
                                <asp:ListItem>january</asp:ListItem>
                                    <asp:ListItem>February</asp:ListItem>
                                    <asp:ListItem>March</asp:ListItem>
                                    <asp:ListItem>April</asp:ListItem>
                                    <asp:ListItem>May</asp:ListItem>
                                    <asp:ListItem>June</asp:ListItem>
                                    <asp:ListItem>July</asp:ListItem>
                                    <asp:ListItem>August</asp:ListItem>
                                    <asp:ListItem>September</asp:ListItem>
                                    <asp:ListItem>October</asp:ListItem>
                                    <asp:ListItem>November</asp:ListItem>
                                    <asp:ListItem>December</asp:ListItem>
                            </asp:DropDownList>
</div>
            </div>
              <div class="place-right">
                        <div class="input-control size-p10 text">
                            <asp:DropDownList ID="ddlyear" runat="server">
                                <asp:ListItem>2020</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2014</asp:ListItem>
                                    <asp:ListItem>2013</asp:ListItem>
                                    <asp:ListItem>2012</asp:ListItem>
                                    <asp:ListItem>2011</asp:ListItem>
                                    <asp:ListItem>2010</asp:ListItem>
                                    <asp:ListItem>2009</asp:ListItem>
                            </asp:DropDownList>
</div>
            </div>
        </div>
        <p class="text-bold text-secondary"> *CCV</p>

        <div class="input-control size-p10 text" data-role="keypad" data-length="3">
   <asp:TextBox ID="txtccv" runat="server"></asp:TextBox>
</div>
        </div>

    </div>
</div>

               <br />
                      <asp:Button ID="btncomplete" OnClick="btncomplete_Click" CssClass="button align-center bg-darkBlue fg-white" runat="server" Text="Complete Checkout" />

         </div>
      
    </div>
</asp:Content>

