﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BlockDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int blockid = int.Parse(Request.QueryString["P"]);
            List<ce831.Block> rec = (new Cls_blocks()).SelectthisIDlist(blockid);
            DataList1.DataSource = rec;
            DataList1.DataBind();

        }
    }

    protected void btnaddtocart_Click(object sender, EventArgs e)
    {
        double Price = double.Parse(((Label)DataList1.Controls[0].FindControl("PriceLabel")).Text);
        string ProductName = ((Label)DataList1.Controls[0].FindControl("NameLabel")).Text;
        string ProductImageUrl = ((Label)DataList1.Controls[0].FindControl("ImageUrlLabel")).Text;
        int procid = int.Parse(((Label)DataList1.Controls[0].FindControl("lblproductid")).Text);
        //int ProductID = int.Parse();
        int amt = int.Parse(((TextBox)DataList1.Controls[0].FindControl("txtamt")).Text);
        if (Profile.SCart == null)
        {
            Profile.SCart = new ShoppingCartExample.Cart();
        }
        Profile.SCart.Insert(procid, Price, amt, ProductName, ProductImageUrl);
        messagebox.Visible = true;
    }
}