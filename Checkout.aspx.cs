﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Checkout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["username"] == null)
            {

                string OriginalUrl = HttpContext.Current.Request.RawUrl;
                string LoginPageUrl = "/Login.aspx";
                HttpContext.Current.Response.Redirect(String.Format("{0}?goto={1}", LoginPageUrl, OriginalUrl));
            }

            ce831.Registration rec = (new Cls_registration()).SelectThisusername(Session["username"].ToString());
            txtaddress.Text = rec.Address1;
            txtemail.Text = rec.Email;
            txtemail.Enabled = false;
            txtfullname.Text = rec.Name;
            txtnumber.Text = rec.MobileNo1;
            txtpostcode.Text = rec.Postcode;
           
        }
        if (Profile.SCart == null)
        {
            Profile.SCart = new ShoppingCartExample.Cart();
        }
        if (!Page.IsPostBack)
        {
            ReBindGrid();
        }
        if (Profile.SCart.Items == null)
        {
            TotalLabel.Visible = false;
        }
    }
    protected void grdCart_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdCart.EditIndex = e.NewEditIndex;
        ReBindGrid();
    }
    protected void grdCart_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        TextBox txtQuantity = (TextBox)grdCart.Rows[e.RowIndex].Cells[3].Controls[0];
        int Quantity = Convert.ToInt32(txtQuantity.Text);
        if (Quantity == 0)
        {
            Profile.SCart.Items.RemoveAt(e.RowIndex);
        }
        else
        {
            Profile.SCart.Items[e.RowIndex].Quantity = Quantity;
        }
        grdCart.EditIndex = -1;
        ReBindGrid();
    }
    protected void grdCart_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdCart.EditIndex = -1;
        ReBindGrid();
    }
    protected void grdCart_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Profile.SCart.Items.RemoveAt(e.RowIndex);
        ReBindGrid();
    }
    private void ReBindGrid()
    {
        grdCart.DataSource = Profile.SCart.Items;
        DataBind();
        TotalLabel.Text = string.Format("Total: {0}", Profile.SCart.Total);
    }
    protected void btncomplete_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtmastercard.Text) || string.IsNullOrEmpty(txtccv.Text) || string.IsNullOrEmpty(txtowner.Text))
        {
            Response.Write("<script>alert('Fill in Card Details');</script>");
            return;
        }


        Cls_Orders A = new Cls_Orders();
        string numbertype = "orderid";
        ce831.order rec = new ce831.order();
        ce831.Autosetting auto = (new Cls_Autosettings()).Getautonumbervalue(numbertype);
        string orderid = string.Format("ORDER/{0}/{1}", DateTime.Now.Year, auto.Nextvalue);
        mod_main.Updateautonumber(numbertype);
        ce831.Registration usereg = (new Cls_registration()).SelectThisusername(Session["username"].ToString());
        foreach (GridViewRow row in grdCart.Rows)
        {

            rec.Orderid = orderid;
            rec.Name = txtfullname.Text;
            rec.Username = usereg.UserName;
            rec.Surname = txtfullname.Text;       
            rec.TransGUID = System.Guid.NewGuid().ToString();
            rec.Address = txtaddress.Text;
            rec.Emailaddress = txtemail.Text;
            rec.Phonenumber = txtnumber.Text;
            rec.State = txtpostcode.Text;
            rec.Userid = usereg.UserID;
            rec.Date = DateTime.Now;
            rec.Itemname = row.Cells[2].Text.ToString();
            rec.Quantity = row.Cells[3].Text.ToString();
            rec.Ordertype = "LAND";
            rec.Price =  decimal.Parse(row.Cells[4].Text);
            rec.Total = decimal.Parse(row.Cells[5].Text);

            mod_main.ResponseInfo res = A.Insert(rec);
            if(res.ErrorCode == 0)
            {
                //Empt the cart after commiting to database and then redirecting to order successful page
                Profile.SCart.Items.Clear();

                Response.Redirect("/Order_successfull.aspx?orderid=" + rec.Orderid + "");
            }
         
        }
    }
}