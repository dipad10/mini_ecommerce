﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
public partial class _Default : System.Web.UI.Page
{
    public static SqlConnection _Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindrepeater(Repeater1);
        }
    }

    private void bindrepeater(Repeater repeater)
    {
        List<ce831.Block> rec = (new Cls_blocks()).SelectAllblocks();
        if (rec.Count == 0)
        {
            lblerror.Visible = true;
            lblerror.Text = "No Data found";
        }
        else
        {
            repeater.DataSource = rec;
            repeater.DataBind();

        }

    }

    protected void btnviewcart_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("/cart.aspx");
    }
}