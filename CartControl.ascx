﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CartControl.ascx.cs" Inherits="CartControl" %>
<h3 class="margin20">My Shopping Cart</h3>
<hr />

<div class="margin20">
    <asp:GridView ID="grdCart" CssClass="table striped hovered" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductID" OnRowCancelingEdit="grdCart_RowCancelingEdit" OnRowDeleting="grdCart_RowDeleting" OnRowEditing="grdCart_RowEditing" OnRowUpdating="grdCart_RowUpdating">
        <AlternatingRowStyle />
        <Columns>
            <asp:TemplateField HeaderText="Serial No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="Image1" Width="50" Height="50" runat="server" ImageUrl='<%# Eval("ImageUrl")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductName" HeaderText="Product" ReadOnly="True" />
            <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
            <asp:BoundField DataField="Price" DataFormatString="{0:N}" HeaderText="Price" ReadOnly="True" />
            <asp:BoundField DataField="SubTotal" DataFormatString="{0:N}" HeaderText="Total"
                ReadOnly="True" />
            <asp:TemplateField HeaderText="Remove">
                <ItemTemplate>
                    <asp:Button ID="Button1" CommandName="Delete" CssClass="button text-small bg-green fg-white mini-button" Font-Size="X-Small" Font-Bold="True" runat="server" Text="Remove" />

                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField>

                <ItemTemplate>
                    <asp:ImageButton ImageUrl="/images/edit-icon.png" CommandName="Edit" runat="server"></asp:ImageButton>
                    <asp:Button ID="Button2" CommandName="Update" CssClass="button text-small bg-green fg-white mini-button" Font-Size="X-Small" Font-Bold="True" runat="server" Text="Update Cart" />
                </ItemTemplate>

            </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="false" ShowEditButton="false" />
        </Columns>
        <EmptyDataTemplate>
            Your Shopping Cart is empty, add items
        <a class="text-small" href="default.aspx">Add Products</a>
        </EmptyDataTemplate>
    </asp:GridView>
</div>
<asp:Label ID="TotalLabel" CssClass="margin20" runat="server" Font-Bold="True" Font-Size="15"></asp:Label>
<br />
<div>
    <div style="margin-right: 20px;" class="place-right">
        <%--								 <a href="checkout.aspx" class="btn btn-sm btn-group btn-default">Checkout <i class="fa fa-check"></i></a>--%>
        <asp:Button ID="btncheckout" Visible="true" Height="29px" OnClick="cmdcheckout_Click" CssClass="button text-small bg-green fg-white mini-button" runat="server" Text="Checkout" Font-Size="X-Small" Font-Bold="True" />

    </div>
    <div style="margin-left: 20px;" class="place-left">
        <%--								 <a href="checkout.aspx" class="btn btn-sm btn-group btn-default">Checkout <i class="fa fa-check"></i></a>--%>
        <asp:Button ID="btncontinueshopping" Visible="true" Height="29px" OnClick="btncontinueshopping_Click" CssClass="button text-small bg-green fg-white mini-button" runat="server" Text="Continue Shopping" Font-Size="X-Small" Font-Bold="True" />

    </div>
    <br />
    <br />
    <br />
</div>

