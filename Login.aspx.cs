﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if(string.IsNullOrEmpty(txtusername.Text) || string.IsNullOrEmpty(txtpassword.Text))
        {
            Response.Write("<script>alert('Fill in Details');</script>");
            return;
        }
        List<ce831.Registration> rec = (new Cls_registration()).Authenticatelogin(txtusername.Text, txtpassword.Text);
        if (rec.Count == 0)
        {
            Response.Write("<script>alert('Invalid Username or Password');</script>");
            return;
        }
        else
        {
            Session["username"] = txtusername.Text;
            string path = Request.QueryString["goto"];
                if(!string.IsNullOrEmpty(path))
            {
                Response.Redirect(path);
            }
                else
            {
                Cls_registration A = new Cls_registration();
                ce831.Registration user = A.SelectThisusername(Session["username"].ToString());
                user.LastAccess = System.DateTime.Now;
                mod_main.ResponseInfo res = A.Update(user);
                if(res.ErrorCode == 0)
                {
                    switch (user.Permission)
                    {
                        
                        case "admin":
                            string urlpath = "/admin";
                            Response.Redirect(string.Format("{0}/home.aspx", urlpath));
                            break;

                        default:
                            string url = "/UserAccount";
                            Response.Redirect(string.Format("{0}/myaccount.aspx", url));
                            break;
                    }
                }

            }
        }
        }
    }
