﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyAccount.aspx.cs" Inherits="UserAccount_My_Account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h5 class="margin20">My Orders</h5>
                        <asp:GridView ID="GridView1" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CssClass="table striped hovered" PageSize="50" runat="server" AllowPaging="True">
                            <Columns>
                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Orderid" HeaderText="#OrderID" SortExpression="Orderid" />

                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Itemname" HeaderText="Description" SortExpression="Itemname" />
                              
                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Price" HeaderText="Price" SortExpression="Price" />
                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Total" HeaderText="Total" SortExpression="Total" />

                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Date" DataFormatString="{0:dd MMM yyyy}" HeaderText="Date" SortExpression="Date" />
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <a href="/Useraccount/order_details.aspx?od=<%#Eval("TransGUID")%>" class=""><i class="icon mif-search"></i></a>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                            </Columns>
                        </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Orders] WHERE ([Username] = @username) ORDER BY [Date] DESC">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="Username" SessionField="username" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
</asp:Content>

