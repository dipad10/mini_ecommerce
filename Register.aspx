﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
       <style>
        .login-form {
            width: 25rem;
          
            /*position: fixed;*/
            /*position:absolute;*/
             margin: 0 auto;
            /*margin-top: -9.375rem;*/
           
            /*margin-left: -12.5rem;*/
            background-color: #ffffff;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
            border-radius:10px;
           
        }
    </style>
    <script>
            $(function () {
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });
        });
    </script>
  
            <div style="box-shadow: 1px 3px 50px 3px gray;" class="login-form animated fadeInUp padding20 block-shadow" style="opacity: 1; transform: scale(1); transition: 0.5s;">

            <h1 class="text-light">Register</h1>
            <hr class="thin">
        <p>Title:</p>
       <div class="input-control full-size select">
           <asp:DropDownList ID="DropDownList1" runat="server">
               <asp:ListItem>MR.</asp:ListItem>
                <asp:ListItem>MRS.</asp:ListItem>
                <asp:ListItem>Miss.</asp:ListItem>
               <asp:ListItem>Dr.</asp:ListItem>
           </asp:DropDownList>
</div>
        <p>First name</p>
        <div class="input-control full-size text">
            <asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox>
</div>
        <p>Lastname</p>
           <div class="input-control full-size text">
            <asp:TextBox ID="txtlastname" runat="server"></asp:TextBox>
</div>

         <p>Username</p>
        <div class="input-control full-size text">
            <asp:TextBox ID="txtusername" runat="server"></asp:TextBox>
</div>
         <p>Password</p>
        <div class="input-control full-size text">
            <asp:TextBox ID="txtpassword" TextMode="Password" runat="server"></asp:TextBox>
</div>
                 <p>E-mail</p>
        <div class="input-control full-size text">
            <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
</div>
        <p>Mobile number</p>
        <div class="input-control full-size text" data-role="keypad">
  <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox>
</div>
      
        <p>PostCode</p>
           <div class="input-control full-size text" data-role="keypad">
            <asp:TextBox ID="txtpostcode" runat="server"></asp:TextBox>
</div>

        <p>Address</p>
           <div class="input-control full-size text">
            <asp:TextBox ID="txtaddress" runat="server"></asp:TextBox>
</div>
        
            <br>
          

            <div class="form-actions">
                <asp:Button ID="Button1" OnClick="Button1_Click" CssClass="button full-size bg-darkBrown rounded fg-white" runat="server" Text="Submit" />

            </div>
            <div style="text-align: center;" class="form-actions">
                <p class="text-small">Already have an account? <a class="text-small link" href="Login.aspx">Login Here</a></p>
            </div>
        </div>
     
</asp:Content>

