﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cls_registration
/// </summary>
public class Cls_registration
{
    ce831.DataClassesDataContext DB;
    public Cls_registration(ce831.DataClassesDataContext Context = null)
    {
        if (Context == null)
        {
            DB = new ce831.DataClassesDataContext();
        }
        else
        {
            DB = Context;
        }
     
    }

    public mod_main.ResponseInfo Insert(ce831.Registration G)
    {
        try
        {
            DB.Registrations.InsertOnSubmit(G);
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }

    public mod_main.ResponseInfo Delete(ce831.Registration G)
    {
        try
        {
            DB.Registrations.DeleteOnSubmit(G);
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }
    public mod_main.ResponseInfo Update(ce831.Registration G)
    {
        try
        {

            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }

    public ce831.Registration SelectThisID(string UserID)
    {
        try
        {
            return (from R in DB.Registrations where R.UserID == UserID select R).ToList()[0];
        }
        catch (Exception ex)
        {
            return new ce831.Registration();

        }
    }

    public ce831.Registration SelectThisusername(string username)
    {
        try
        {
            return (from R in DB.Registrations where R.UserName == username select R).ToList()[0];
        }
        catch (Exception ex)
        {
            return new ce831.Registration();

        }
    }
    public List<ce831.Registration> SelectthisIDlist(string userid)
    {
        try
        {
            return (from U in DB.Registrations where U.UserID == userid select U).ToList();
        }
        catch (Exception ex)
        {
            return new List<ce831.Registration>();
        }
    }
    public List<ce831.Registration> Authenticatelogin(string username, string password)
    {
        try
        {
            return (from R in DB.Registrations where R.UserName == username & R.Password == password select R).ToList();
        }
        catch (Exception ex)
        {
            return new List<ce831.Registration>();

        }
    }
    public List<ce831.Registration> SelectAllUsers()
    {
        try
        {
            return (from U in DB.Registrations select U).ToList();
        }
        catch (Exception ex)
        {
            return new List<ce831.Registration>();
        }
    }
}