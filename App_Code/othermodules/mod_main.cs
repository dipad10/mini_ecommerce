﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Web.Mail;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.IO.Ports;
using System.Collections.Specialized;

public static class mod_main
{

    //reference this m_strconnString anywhere u want to call ur connection into d DB cos its now made public


    public static schoolsettings settings;
    public enum ErrCodeEnum
    {
        NO_ERROR = 0,
        INVALID_GUID = 200,
        INVALID_PASSWORD = 200,
        FORBIDDEN = 407,
        INVALID_RANGE = 408,
        ACCOUNT_LOCKED = 400,
        ACCOUNT_EXPIRED = 400,
        GENERIC_ERROR = 100
    }

    public class ResponseInfo
    {
        public int ErrorCode;
        public string ErrorMessage;
        public string ExtraMessage;
        public int TotalSuccess;
        public int TotalFailure;
        public int TotalCharged;
        public int CurrentBalance;
    }

    #region "GET FUNCTIONS"

    //public static string getQueryString(NameValueCollection q = null, string SkipParam = "")
    //{
    //    if (q == null)
    //        q = My.Request.QueryString;
    //    string query = "";
    //    string[] skips = Strings.Split(SkipParam, ",");

    //    for (int p = 0; p <= q.Count - 1; p++)
    //    {
    //        if (!skips.Contains(q.GetKey(p)))
    //        {
    //            query += "&" + q.GetKey(p) + "=" + string.Join("", q.GetValues(p));
    //        }
    //    }

    //    if (query.StartsWith("&"))
    //        query = query.Remove(0, 1);
    //    return query;
    //}


    //public static string DecodeFromBase64String(string inputStr)
    //{
    //    try
    //    {
    //        if (Strings.Len(inputStr) > 0 & (Strings.Len(inputStr) % 4) > 0)
    //        {
    //            inputStr = inputStr + Strings.Left("====", (4 - (Strings.Len(inputStr) % 4)));
    //        }
    //        // Convert the binary input into Base64 UUEncoded output.
    //        byte[] binaryData = System.Convert.FromBase64String(inputStr);
    //        return (new System.Text.UnicodeEncoding()).GetString(binaryData);
    //    }
    //    catch (System.ArgumentNullException exp)
    //    {
    //        return "";
    //    }
    //    catch (Exception ex)
    //    {
    //        return "";
    //    }
    //}
    public struct schoolsettings
    {
        public int id;
        public string name;
        public string logo;
        public string address;

        public string website;
    }

    //public static void Messagebox(UI.Page Page, string message)
    //{
    //    Page.Response.Cookies.Add(new HttpCookie("msgbox", message));
    //}
    //public static Data.DataTable getAdhocTable(string strSQL, Data.Common.DbConnection con)
    //{
    //    SqlCommand cmd = new SqlCommand(strSQL, con);
    //    var _with1 = cmd;
    //    _with1.CommandType = CommandType.Text;

    //    SqlDataAdapter rdr = new SqlDataAdapter(cmd);
    //    DataTable tbl = new DataTable("Table1");
    //    rdr.Fill(tbl);
    //    return tbl;
    //}
    #endregion

    #region "CORE SYSTEM FUNCTIONS"

    public static ResponseInfo _GetResponseStruct(ErrCodeEnum ErrCode, int TotalSuccess = 0, int TotalFailure = 0, string ErrorMsg = "", string ExtraMsg = "", int currentBalance = 0)
    {
        ResponseInfo res = new ResponseInfo();
        var _with2 = res;
        _with2.ErrorCode = Convert.ToInt32(ErrCode);
        _with2.ErrorMessage = ErrorMsg;
        _with2.ExtraMessage = ExtraMsg;
        _with2.TotalSuccess = TotalSuccess;
        _with2.TotalFailure = TotalFailure;
        _with2.CurrentBalance = currentBalance;
        return res;
    }
    public static bool Updateautonumber(string numbertype)
    {
        try
        {
            Cls_Autosettings A = new Cls_Autosettings();
            ce831.Autosetting rec = A.Getautonumbervalue(numbertype);
            rec.Nextvalue = rec.Nextvalue + 1;
            ResponseInfo res = A.Update(rec);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }


    }
    #endregion
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
