﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cls_Orders
/// </summary>
public class Cls_Orders
{
    ce831.DataClassesDataContext DB;
    public Cls_Orders(ce831.DataClassesDataContext Context = null)
    {
        if (Context == null)
        {
            DB = new ce831.DataClassesDataContext();
        }
        else
        {
            DB = Context;
        }
        //
        // TODO: Add constructor logic here
        //
    }
    public mod_main.ResponseInfo Insert(ce831.order G)
    {
        try
        {
            DB.orders.InsertOnSubmit(G);
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }

    public mod_main.ResponseInfo Delete(ce831.order G)
    {
        try
        {
            DB.orders.DeleteOnSubmit(G);
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }
    public mod_main.ResponseInfo Update(ce831.order G)
    {
        try
        {

            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }

    public ce831.order SelectThisID(string orderid)
    {
        try
        {
            return (from R in DB.orders where R.Orderid == orderid select R).ToList()[0];
        }
        catch (Exception ex)
        {
            return new ce831.order();

        }
    }


          public ce831.order SelectThisSN(int SN)
    {
        try
        {
            return (from R in DB.orders where R.SN == SN select R).ToList()[0];
        }
        catch (Exception ex)
        {
            return new ce831.order();
             
        }
    }
    public List<ce831.order> SelectthisIDlist(string orderid)
    {
        try
        {
            return (from U in DB.orders where U.Orderid == orderid select U).ToList();
        }
        catch (Exception ex)
        {
            return new List<ce831.order>();
        }
    }
    public List<ce831.order> SelectAllorders()
    {
        try
        {
            return (from U in DB.orders select U).ToList();
        }
        catch (Exception ex)
        {
            return new List<ce831.order>();
        }
    }
}
