﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cls_Autosettings
/// </summary>
public class Cls_Autosettings
{
    ce831.DataClassesDataContext DB;
    public Cls_Autosettings(ce831.DataClassesDataContext Context = null)
    {
        if (Context == null)
        {
            DB = new ce831.DataClassesDataContext();
        }
        else
        {
            DB = Context;
        }
        //
        // TODO: Add constructor logic here
        //
    }

    public mod_main.ResponseInfo Insert(ce831.Autosetting G)
    {
        try
        {
            DB.Autosettings.InsertOnSubmit(G);
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }

    public mod_main.ResponseInfo Delete(ce831.Autosetting G)
    {
        try
        {
            DB.Autosettings.DeleteOnSubmit(G);
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }
    public mod_main.ResponseInfo Update(ce831.Autosetting G)
    {
        try
        {

            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }

    public ce831.Autosetting SelectThisID(int SN)
    {
        try
        {
            return (from R in DB.Autosettings where R.SN == SN select R).ToList()[0];
        }
        catch (Exception ex)
        {
            return new ce831.Autosetting();

        }
    }
    public ce831.Autosetting Getautonumbervalue(string numbertype)
    {
        try
        {
            return (from R in DB.Autosettings where R.NumberType == numbertype select R).ToList()[0];
        }
        catch (Exception ex)
        {
            return new ce831.Autosetting();

        }
    }


}