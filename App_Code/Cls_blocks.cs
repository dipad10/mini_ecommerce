﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

/// <summary>
/// Summary description for Cls_blocks
/// </summary>
public class Cls_blocks
{
    ce831.DataClassesDataContext DB;
    public Cls_blocks(ce831.DataClassesDataContext Context = null)
    {
        if (Context == null)
        {
            DB = new ce831.DataClassesDataContext();
        }
        else
        {
            DB = Context;
        }
        //
        // TODO: Add constructor logic here
        //
    }
    public mod_main.ResponseInfo Insert(ce831.Block G)
    {
        try
        {
            DB.Blocks.InsertOnSubmit(G);
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }

    public mod_main.ResponseInfo Delete(ce831.Block G)
    {
        try
        {
            DB.Blocks.DeleteOnSubmit(G);
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }
    public mod_main.ResponseInfo Update(ce831.Block G)
    {
        try
        {
           
            DB.SubmitChanges();
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.NO_ERROR, 1, 0);
        }
        catch (Exception ex)
        {
            return mod_main._GetResponseStruct(mod_main.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
        }

    }

    public ce831.Block SelectThisID(int productID)
    {
        try
        {
            return (from R in DB.Blocks where R.ProductID == productID select R).ToList()[0];
        }
        catch (Exception ex)
        {
            return new ce831.Block();

        }
    }
    public List<ce831.Block> SelectthisIDlist(int productID)
    {
        try
        {
            return (from U in DB.Blocks where U.ProductID == productID select U).ToList();
        }
        catch (Exception ex)
        {
            return new List<ce831.Block>();
        }
    }
    public List<ce831.Block> SelectAllblocks()
    {
        try
        {
            return (from U in DB.Blocks select U).ToList();
        }
        catch (Exception ex)
        {
            return new List<ce831.Block>();
        }
    }
}