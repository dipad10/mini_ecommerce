﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if(dovalidation() == true)
        {
            Cls_registration P = new Cls_registration();

            string numbertype = "Userid";
            ce831.Autosetting auto = (new Cls_Autosettings()).Getautonumbervalue(numbertype);
            string userid = string.Format("USER/{0}/{1}", DateTime.Now.Year, auto.Nextvalue);
            mod_main.Updateautonumber(numbertype);

            ce831.Registration Rec = new ce831.Registration();
            Rec.Name = txtfirstname.Text.ToString() + " " + txtlastname.Text;
            Rec.Title = DropDownList1.SelectedValue;
            Rec.MobileNo1 = txtnumber.Text;
            Rec.MobileNo2 = txtnumber.Text;
            Rec.Postcode = txtpostcode.Text;
            Rec.Address1 = txtaddress.Text;
            Rec.Address2 = txtaddress.Text;
            Rec.Email = txtemail.Text;
            Rec.Permission = "user";
            Rec.UserName = txtusername.Text;
            Rec.Password = txtpassword.Text;
            Rec.UserID = userid;
            Rec.CreatedOn = DateTime.Now;
            Rec.Active = "1";
            mod_main.ResponseInfo res = P.Insert(Rec);
            if (res.ErrorCode == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "alert", "alert('Registration Successful!');window.location='Login.aspx';", true);
            }
            else
            {
                Response.Write("<script>alert('"+ res.ErrorMessage + " "+ res.ExtraMessage + "');</script>");
            }
                


        }
    }
    private Boolean dovalidation()
    {
        if (string.IsNullOrEmpty(txtfirstname.Text))
        {
            Response.Write("<script>alert('Enter first name');</script>");
            return false;    
        }
        else if(string.IsNullOrEmpty(txtlastname.Text))
        {
            Response.Write("<script>alert('Enter Last name');</script>");
            return false;
        }
        else if (string.IsNullOrEmpty(txtnumber.Text))
        {
            Response.Write("<script>alert('Enter number');</script>");
            return false;
        }
        else if (string.IsNullOrEmpty(txtpassword.Text))
        {
            Response.Write("<script>alert('Enter password');</script>");
            return false;
        }
        else if (string.IsNullOrEmpty(txtaddress.Text))
        {
            Response.Write("<script>alert('Enter address');</script>");
            return false;
        }
        else if (string.IsNullOrEmpty(txtpostcode.Text))
        {
            Response.Write("<script>alert('Enter postcode');</script>");
            return false;
        }
        else if (string.IsNullOrEmpty(txtusername.Text))
        {
            Response.Write("<script>alert('Enter username');</script>");
            return false;
        }
        return true;
        
    }
}