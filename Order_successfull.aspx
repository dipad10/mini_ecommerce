﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Order_successfull.aspx.cs" Inherits="Order_successfull" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 style="margin:0 auto; text-align:center;" class="title fg-darkOrange">
        Order Completed Successfully!
    </h1>
    <br />
    <div style="margin:0 auto; text-align:center;">
            <asp:Button ID="Button1" CssClass="button bg-darkOrange align-center fg-white btn-max" runat="server" Text="Shop Again" />

    </div>
</asp:Content>

